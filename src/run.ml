module Old = Dag_old.Dag.Make (struct
    type t = int
  end)

module New = Dag.Make (struct
    type t = int
  end)

module New_compute_cycle = Dag_compute_cycle.Dag.Make (struct
    type t = int
  end)

let rec loop g add_edge nodes = function
  | [] -> ()
  | (x,y)::ops ->
    let res = add_edge g nodes.(x) nodes.(y) in
    if res then loop g add_edge nodes ops

(*
let run ~g ~add_edge ~create_node (maxnode, ops) =
  let dummy = create_node g (-1) in
  let nodes = Array.make (maxnode+1) dummy in
  let alloc_node x =
    let n = nodes.(x) in
    if n == dummy then (
      let n = create_node g x in
      nodes.(x) <- n
    )
  in
  List.iter (fun (x, y) -> alloc_node x; alloc_node y) ops;
  let nodes_marshalled = Marshal.to_bytes nodes [] in
  fun () ->
    let nodes : _ array = Marshal.from_bytes nodes_marshalled 0 in
    loop g add_edge nodes ops
*)

let run ~g ~add_edge ~create_node (maxnode, ops) =
  let dummy = create_node g (-1) in
  let nodes = Array.make (maxnode+1) dummy in
  let alloc_node x =
    let n = nodes.(x) in
    if n == dummy then (
      let n = create_node g x in
      nodes.(x) <- n
    )
  in
  List.iter (fun (x, y) -> alloc_node x; alloc_node y) ops;
  loop g add_edge nodes ops

let run_base_old ops =
  let g = Old.create () in
  let add_edge _ _ _ = true in
  run ~g ~add_edge ~create_node:(fun g x -> Old.{ data = x; info = Old.create_node_info g }) ops

let run_base_new ops =
  let g = New_compute_cycle.create () in
  let add_edge _ _ _ = true in
  run ~g ~add_edge ~create_node:(fun g x ->
    New_compute_cycle.{ data = x; info = New_compute_cycle.create_node_info g }) ops

let run_old ops =
  let g = Old.create () in
  let add_edge g u v =
    try Old.add g u v; true with Old.Cycle _ -> false in
  run
    ~g ~add_edge
    ~create_node:(fun g x -> Old.{ data = x; info = Old.create_node_info g })
    ops

let run_new ops =
  let g = New.create () in
  let add_edge g u v =
    try New.add ~delta:false g u v; true with New.Cycle _ -> false in
  run
    ~g ~add_edge
    ~create_node:(fun g x -> New.{ data = x; info = New.create_node_info g })
    ops

let run_new_delta ops =
  let g = New.create () in
  let add_edge g u v =
    try New.add ~delta:true g u v; true with New.Cycle _ -> false in
  run
    ~g ~add_edge
    ~create_node:(fun g x -> New.{ data = x; info = New.create_node_info g })
    ops

let run_new_compute_cycle ops =
  let g = New_compute_cycle.create () in
  let add_edge g u v =
    try New_compute_cycle.add ~delta:false g u v; true
    with New_compute_cycle.Cycle _ -> false in
  run
    ~g ~add_edge
    ~create_node:(fun g x ->
      New_compute_cycle.{ data = x; info = New_compute_cycle.create_node_info g })
    ops

let run_new_compute_cycle_delta ops =
  let g = New_compute_cycle.create () in
  let add_edge g u v =
    try New_compute_cycle.add ~delta:true g u v; true
    with New_compute_cycle.Cycle _ -> false in
  run
    ~g ~add_edge
    ~create_node:(fun g x ->
      New_compute_cycle.{ data = x; info = New_compute_cycle.create_node_info g })
    ops

open Core_bench.Std

let () =
  let trace = "ocaml-platform.trace" in
  let ic = open_in trace in
  let rec loop (maxnode, ops) =
    match input_line ic with
    | exception End_of_file -> (maxnode, ops)
    | line ->
      let (x, y) = Scanf.sscanf line "%d %d" (fun x y -> (x, y)) in
      loop (max (max maxnode x) y, (x, y) :: ops)
  in
  let (maxnode, ops) = loop (0, []) in
  let ops = List.rev ops in

  Core.Command.run (Bench.make_command [
    Bench.Test.create ~name:"baseline_cur"
      (fun () -> run_base_old (maxnode, ops));
    Bench.Test.create ~name:"baseline_ver"
      (fun () -> run_base_new (maxnode, ops));
    Bench.Test.create ~name:"current"
      (fun () -> run_old (maxnode, ops));
    Bench.Test.create ~name:"verified"
      (fun () -> run_new_compute_cycle (maxnode, ops));
  ])
