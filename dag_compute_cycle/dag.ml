include Dag_intf

module Make(Value : Value) : S with type value := Value.t = struct
  module Raw_graph = struct
    type mark = int

    type t = {
      mutable fresh_id : int; (* only used for printing... *)
      mutable fresh_mark : int;

      mutable num : int;
      mutable arcs : int;
    }
    type graph = t

    type node_info = {
      id : int;
      mutable mark : mark;
      mutable level : int;
      mutable deps : node list;
      mutable rev_deps : node list;
      mutable parent : node option;
    }

    and node =
      { data : Value.t
      ; info : node_info
      }

    type vertex = node

    let new_mark g =
      let m = g.fresh_mark in
      g.fresh_mark <- g.fresh_mark + 1;
      m

    let vertex_eq v1 v2 = v1 == v2
    let is_marked _ v m = v.info.mark = m
    let set_mark _ v m = v.info.mark <- m
    let get_level _ v = v.info.level
    let set_level _ v l = v.info.level <- l
    let get_incoming _ v = v.info.rev_deps
    let clear_incoming _ v = v.info.rev_deps <- []
    let add_incoming _ v w = v.info.rev_deps <- w::v.info.rev_deps
    let get_outgoing _ v = v.info.deps
    let get_parent _ v =
      match v.info.parent with None -> assert false | Some v -> v
    let set_parent _ v p = v.info.parent <- Some p
    let raw_add_edge _ v w =
      v.info.deps <- w::v.info.deps;
      if v.info.level = w.info.level then w.info.rev_deps <- v::w.info.rev_deps
    let raw_add_vertex _ _ = ()
    let nb_vertices g = g.num
    let nb_edges g = g.arcs
  end

  include Raw_graph

  module IC = Incremental_cycles_compute_cycle.Incremental_cycles.Make(Raw_graph)

  exception Cycle of node list

  let create () =
    {
      fresh_id = 0;
      fresh_mark = 0;

      num = 0;
      arcs = 0;
    }

  let create_node_info g =
    let id = g.fresh_id in
    g.fresh_id <- g.fresh_id + 1;
    g.num <- g.num + 1;
    {
      id;
      mark = -1;
      level = 1;
      deps = [];
      rev_deps = [];
      parent = None;
    }

  let add ~delta g v w =
    g.arcs <- g.arcs + 1;
    match IC.add_edge_or_detect_cycle ~delta g v w with
    | IC.EdgeAdded -> ()
    | IC.EdgeCreatesCycle f -> raise (Cycle (f()))

  let children node = node.info.deps

  let is_child v w =
    v.info.deps |> ListLabels.exists ~f:(fun c -> c.info.id = w.info.id)
end
